= Free Market Health Care
Tristan Juricek <mr.tristan@gmail.com>
2018-04-15
:tags: technology
:keywords: Tristan Juricek,technology
:description: Reflecting on how the profit motives and health care do not mix well. \
    This is just a single case study from Goldman Sachs.

Apparently Goldman Sachs questioned how profitable it is to, you know, perform health care miracles: +++
<label for="sn-thingy" class="margin-toggle sidenote-number"></label>
<input type="checkbox" id="sn-thingy" class="margin-toggle"/>
<span class='sidenote'><a href="https://www.cnbc.com/2018/04/11/goldman-asks-is-curing-patients-a-sustainable-business-model.html
">
Goldman Sachs asks in biotech research report: 'Is curing patients a sustainable business model?'
</a></span>+++

[quote]
____
Richter cited Gilead Sciences' treatments for hepatitis C, which achieved cure rates of more than 90 percent.
The company's U.S. sales for these hepatitis C treatments peaked at $12.5 billion in 2015, but have been falling ever since.
Goldman estimates the U.S. sales for these treatments will be less than $4 billion this year, according to a table in the report.
____

Now, I know it's common to vilify investment firms as pushing profits over everything, but seriously, this is what their job is.
If they didn't question profitability, they would not be trusted with making investments.
I don't believe this report is pushing an agenda, or is particularly nefarious.
What it highlights, however, is that free markets can not always be trusted to make the best decisions.

The report Goldman created is a focused analysis of investing in the healthcare industry.
This isn't a taking on broad scope, answering questions like, "Hey! Where do I put my money?"
This is detailed analysis from dedicated teams researching a specific facet of our economy.
It is not their job to question how best society to should take care of it's people.
They are there to maximize profits within the specialization they are dedicated to.

But, if Goldman's analysis causes healthcare companies to lose money because they may not maximize profitability, who is to blame?
Is it Goldman's job to hold back this information because it's not true?
That road leads to dark places, like decisions made behind closed doors.

Ultimately, we need a way where companies can possibly provide solutions for the greater good, and yet, not be penalized by investors.
But what we can't do, is assume that the free market will simply sort itself out.
Short term profit seeking will frequently trump long term interests that may in fact yield other, non-financial benefits.
Like living a long, happy life.

Ideally, our public representatives own this responsibility.
But they do not appear ready or capable of solving much these days.
The American government can barely pass budgets, let alone make the major changes required to stabilize healthcare costs.

If the situation continues to deteriorate because we can not restrict profit seeking by the American healthcare industry, it may be our interconnected world that leads the way.
We have reached a point, where it may be cheaper for American citizens to seek major procedures via hospitals internationally, in places like Costa Rica and China.
And there's very little evidence that it's any riskier for them to do so.

So when I read about an investment firm looking like a greedy villain, I don't throw up my arms in desperation.
It's merely another incident challenging our current social order.
Eventually, changes will happen.
New markets will emerge, and those markets may not care about our system of nations.
Time will tell if we are headed to chaos, or merely a smarter global system.

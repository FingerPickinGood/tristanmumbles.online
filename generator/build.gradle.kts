import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }
}


plugins {
    application
    id("org.jetbrains.kotlin.jvm")
}

extra["kotlin.version"] = plugins.getPlugin(KotlinPluginWrapper::class.java).kotlinPluginVersion

application {
    mainClassName = "io.ktor.server.netty.DevelopmentEngine"
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("org.jetbrains.kotlin:kotlin-reflect")

    compile("com.rometools:rome:1.9.0")
    compile("com.github.dfabulich:sitemapgen4j:1.0.6")

    compile("org.asciidoctor:asciidoctorj:1.6.0")
    compile("org.asciidoctor:asciidoctorj-pdf:1.5.0-alpha.11")
}
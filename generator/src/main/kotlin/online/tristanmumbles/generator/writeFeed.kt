package online.tristanmumbles.generator

import com.rometools.rome.feed.synd.SyndContentImpl
import com.rometools.rome.feed.synd.SyndEntryImpl
import com.rometools.rome.feed.synd.SyndFeedImpl
import com.rometools.rome.feed.synd.SyndLinkImpl
import com.rometools.rome.io.SyndFeedOutput
import java.io.File
import java.time.ZoneOffset
import java.util.*

fun writeFeed(articles: List<Article>, rootDir: File) {

    val feed = SyndFeedImpl()
    feed.feedType = "atom_1.0"
    feed.title = "tristanmumbles.online"
    feed.link = "http://www.tristanmumbles.online"
    feed.publishedDate = Date()
    feed.description = "Personal weblog of Tristan Juricek"

    feed.links.add(SyndLinkImpl().apply {
        rel = "self"
        type = "application/atom+xml"
        href = "http://www.tristanmumbles.online/atom.xml"
    })

    val entries = articles.map { article ->
        val entry = SyndEntryImpl()

        entry.title = article.document.doctitle()
        entry.link = "${feed.link}/${article.htmlTargetPath}"
        entry.publishedDate = Date.from(article.document.revdate.atStartOfDay().toInstant(ZoneOffset.UTC))

        entry.description = SyndContentImpl().apply {
            type = "text/html"
            value = "<h1>${article.document.doctitle()}</h1>" + article.document.convert()
        }

        entry
    }

    feed.entries = entries

    val output = SyndFeedOutput()

    val atomFile = File(rootDir, "atom.xml")
    atomFile.writer().use { writer ->
        output.output(feed, writer)
    }
}
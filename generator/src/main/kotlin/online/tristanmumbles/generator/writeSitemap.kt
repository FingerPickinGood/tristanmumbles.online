package online.tristanmumbles.generator

import com.redfin.sitemapgenerator.WebSitemapGenerator
import java.io.File

fun writeSitemap(articles: List<Article>, rootDir: File) {

    val rootUrl = "http://www.tristanmumbles.online"

    val generator = WebSitemapGenerator(rootUrl, rootDir)

    generator.addUrl("$rootUrl/index.html")
    generator.addUrl("$rootUrl/contact.html")

    articles.forEach {
        generator.addUrl("$rootUrl/${it.htmlTargetPath}")
    }

    val siteMaps = generator.write()

    siteMaps.forEach { println("wrote sitemap: $it") }
}
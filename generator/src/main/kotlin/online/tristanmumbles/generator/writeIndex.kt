package online.tristanmumbles.generator

import org.asciidoctor.OptionsBuilder
import org.asciidoctor.SafeMode
import org.asciidoctor.ast.*
import java.io.File
import java.time.format.DateTimeFormatter

private class ArticleAppender(val content: StringBuilder) {

    private var count = 1

    private val dateFormatter = DateTimeFormatter.ofPattern("MMM dd, yyyy")

    fun writeArticles(articles: List<Article>) {
        articles.forEach { article ->
            content.append("""
                - link:${article.htmlTargetPath}[${article.document.doctitle()}], ${dateFormatter.format(article.document.revdate)}
            """.trimIndent())
            content.append("\n")

            count++
        }
    }
}


fun writeIndex(tagToArticles: Map<String, List<Article>>, rootDir: File) {

    val content = StringBuilder("""
        = Tristan Mumbles

        Organized by major topic, then date:
    """.trimIndent())
    content.append("\n\n")

    val articleAppender = ArticleAppender(content)

    content.append("""
        == Woodworking
    """.trimIndent())
    content.append("\n\n")

    articleAppender.writeArticles(tagToArticles["woodworking"] ?: emptyList())

    content.append("\n")
    content.append("""
        == Technology
    """.trimIndent())
    content.append("\n\n")

    articleAppender.writeArticles(tagToArticles["technology"] ?: emptyList())

    content.append("\n")
    content.append("""
        == Kotlin
    """.trimIndent())
    content.append("\n\n")

    articleAppender.writeArticles(tagToArticles["kotlin"] ?: emptyList())

    content.append("\n")
    content.append("""
        == Spring
    """.trimIndent())
    content.append("\n\n")

    articleAppender.writeArticles(tagToArticles["spring"] ?: emptyList())

    content.append("\n")
    content.append("""
        == Gradle
    """.trimIndent())
    content.append("\n\n")

    articleAppender.writeArticles(tagToArticles["gradle"] ?: emptyList())

    val outputAdoc = File(rootDir, "index.adoc")
    outputAdoc.bufferedWriter().use { it.write(content.toString()) }

    val options = OptionsBuilder.options()
            .inPlace(false)
            .safe(SafeMode.UNSAFE)
            .templateDir(File("./generator/src/main/templates/index"))
            .asMap()

    asciidoctor.convertFile(outputAdoc, options)
}


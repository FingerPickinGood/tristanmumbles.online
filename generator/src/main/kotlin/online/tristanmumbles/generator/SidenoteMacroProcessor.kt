package online.tristanmumbles.generator

import org.asciidoctor.ast.ContentNode
import org.asciidoctor.extension.InlineMacroProcessor

class SidenoteMacroProcessor(configName: String, options: MutableMap<String, Any>) : InlineMacroProcessor(configName, options) {

    constructor(configName: String) : this(configName, mutableMapOf())

    override fun process(parent: ContentNode?, target: String?, attributes: MutableMap<String, Any>?): Any {
        val id = target ?: "unknown"
        val text = attributes!!["1"] as String

        return """
            <label for="sn-$id" class="margin-toggle sidenote-number"></label>
                <input type="checkbox" id="sn-$id" class="margin-toggle"/>
            <span class='sidenote'>$text</span>
        """.trimIndent()
    }

}
package online.tristanmumbles.generator

import org.asciidoctor.Asciidoctor

val asciidoctor: Asciidoctor by lazy {
    val asciidoctor = Asciidoctor.Factory
            .create()

    asciidoctor.javaExtensionRegistry()
            .inlineMacro("newthought", NewthoughtMacroProcessor::class.java)
            .inlineMacro("sidenote", SidenoteMacroProcessor::class.java)

    asciidoctor
}


package online.tristanmumbles.generator

import org.asciidoctor.OptionsBuilder
import org.asciidoctor.SafeMode
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

/**
 * Just convert each output file into the build directory
 */
fun writeIndividualArticles(articles: List<Article>, rootDir: File) {

    val options = OptionsBuilder.options()
            .inPlace(false)
            .safe(SafeMode.UNSAFE)
            .templateDir(File("./generator/src/main/templates/article"))
            .asMap()

    articles.forEach { article ->
        // Yeah, so asciidoctor doesn't really do a nice job of documenting it's quirks.
        // This doesn't really return the content, just generates an HTML file
        asciidoctor.convertFile(article.localFile, options)
        val targetPath = File(rootDir, article.htmlTargetPath).toPath()

        if (!targetPath.parent.toFile().exists()) {
            targetPath.parent.toFile().mkdirs()
        }

        Files.move(article.htmlSourceFile.toPath(), targetPath, StandardCopyOption.REPLACE_EXISTING)
    }
}
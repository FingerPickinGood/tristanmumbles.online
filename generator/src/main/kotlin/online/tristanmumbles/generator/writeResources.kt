package online.tristanmumbles.generator

import java.io.File

fun writeResources(rootDir: File) {
    File("generator/src/main/resources").copyRecursively(rootDir, overwrite = true)
}
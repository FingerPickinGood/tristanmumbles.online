package online.tristanmumbles.generator

import org.asciidoctor.ast.ContentNode
import org.asciidoctor.extension.InlineMacroProcessor

/**
 * Usage:
 *
 * <code>newthought:id[Here is some text]</code>
 *
 * <p> The id part is basically ignored but required by asciidoc.
 */
class NewthoughtMacroProcessor(name: String, config:MutableMap<String, Any>) : InlineMacroProcessor(name, config) {

    constructor(name:String): this(name, mutableMapOf())

    override fun process(parent: ContentNode?, target: String?, attributes: MutableMap<String, Any>?): Any {

        val text = attributes!!["1"] as String

        return """
            <span id="$target" class="newthought">$text</span>
        """.trimIndent()
    }
}
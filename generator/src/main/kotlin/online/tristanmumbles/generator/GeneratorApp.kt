package online.tristanmumbles.generator

import java.io.File

fun main(args: Array<String>) {
    val rootDir = File("build/site")
    if (!rootDir.exists()) {
        rootDir.mkdirs()
    }

    writeResources(rootDir)

    val articles = readArticles("./articles")

    println("articles: ")

    val tagToArticles : MutableMap<String, MutableList<Article>> = mutableMapOf()

    articles.forEach { article ->
        val tags = (article.document.attributes["tags"] as String?)?.split("\\s+".toRegex()) ?: emptyList()
        tags.forEach { tag ->
            tagToArticles.putIfAbsent(tag, mutableListOf(article))?.let {list ->
                list.add(article)
            }
        }
    }

    tagToArticles.forEach { tag, articles ->
        println("tag: $tag\n")
        articles.forEach {
            println("  title: ${it.document.doctitle()}")
            println("  revdate: ${it.document.revdate}\n")
        }
    }

    writeIndividualArticles(articles, rootDir)
    writeIndex(tagToArticles, rootDir)
    writeFeed(articles, rootDir)
    writeSitemap(articles, rootDir)
}

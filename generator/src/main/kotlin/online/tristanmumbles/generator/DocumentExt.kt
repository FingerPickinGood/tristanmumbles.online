package online.tristanmumbles.generator

import org.asciidoctor.ast.Document
import java.time.LocalDate
import java.time.format.DateTimeFormatter

val Document.revdate: LocalDate
    get() {
        val dateStr: String = (attributes["revdate"] ?: IllegalStateException("revdate missing in document")) as String
        return LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE)
    }


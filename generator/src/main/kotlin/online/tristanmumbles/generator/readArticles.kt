package online.tristanmumbles.generator

import org.asciidoctor.AsciiDocDirectoryWalker
import org.asciidoctor.ast.Document
import java.io.File
import java.time.format.DateTimeFormatter

fun readArticles(path: String): List<Article> {
    val files = AsciiDocDirectoryWalker(path).scan()
    return files.map { file ->
        val doc = asciidoctor.loadFile(file, mapOf())
        Article(file, doc)
    }.sortedWith(compareBy<Article>({ it.document.revdate }, { it.localFile.lastModified() }, {it.document.doctitle()}).reversed())
}

data class Article(val localFile: File, val document: Document) {

    companion object {
        val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    }

    val htmlSourceFile : File = File(localFile.parentFile, localFile.nameWithoutExtension + ".html")

    val htmlTargetPath : String = "${dateFormatter.format(document.revdate)}/${htmlSourceFile.name}"
}

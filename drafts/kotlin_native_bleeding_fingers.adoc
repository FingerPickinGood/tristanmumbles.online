= Kotlin/Native Useful?

When trying to even run tutorials... still... I'm running immediately into Gradle shenanigans.

The konan plugin is tied to gradle 5.0.
When using 5.1.1, I received this error:

....
> Could not create an instance of type org.jetbrains.kotlin.gradle.plugin.konan.KonanInteropLibrary_Decorated.
   > org.gradle.api.internal.DefaultNamedDomainObjectSet.<init>(Ljava/lang/Class;Lorg/gradle/internal/reflect/Instantiator;Lorg/gradle/api/Namer;)V
....

Had this issue before: their code compiles against a specific version of gradle.

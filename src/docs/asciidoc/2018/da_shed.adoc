= Da Shed
Tristan Juricek <mr.tristan@gmail.com>
2018-12-02
:tags: woodworking
:keywords: Tristan Juricek,woodworking,shed
:description: The woodworking shed I built fall 2018. It's small, just 8x12.
:image: https://tristanmumbles.online/img/da_shed/17_door_and_window.jpg

I went absolutely nutty this fall, and decided to build a woodworking shed in my back yard.
Not only that, it's a mini-shop.
Eight by twelve feet.

Since Home Depot's "pro" department basically forgot to schedule the delivery, I had an entire day to prepare the ground.
Which I ended up needing anyway.

I started by marking things off.

image::/img/da_shed/01_it-begins.jpg[It Begins,width=640,height=480]

Then I went through and just dug up about 1"-2" of soil.
Which is what ended up taking a long time.

image::/img/da_shed/02_digging.jpg[Dig It Up,width=640,height=480]

And then I removed some soil.

image::/img/da_shed/03_removed_some.jpg[Removed Some Dirt,width=640,height=480]

You might be thinking "hey, that's not that much dirt".
And that's what I thought too, but it ended up being about one and a half yards... i.e., a lot.
Enough to require a truck with serious hauling capacity to take away.

image::/img/da_shed/04_dirt_bags.jpg[Dirt Bags,width=640,height=480]

That dirt up there? It was like half the amount I removed.

Once the extra soil was taken away, I was able to properly level the soil for a stable base.

image::/img/da_shed/05_flattening.jpg[Flattening,width=640,height=480]

And on top of this flat soil... weed blocker and some sand.

image::/img/da_shed/06_weeds.jpg[Weed Blocker,width=640,height=480]

Now, the next day, Home Depot finally delivered everything.
Which meant I spent most of the day lifting and moving things from my driveway... to the back yard.

So the third day, I could _really_ get started by laying down some skids.

image::/img/da_shed/07_skids.jpg[Skids,width=640,height=480]

Then, I built the frame using 2x6 wood.
I went a little crazy here, only the skids really needed to be treated.
And even with the skids on top of concrete blocks, they barely touch water.

Now, the gap underneath the frame is significant here - need that air flow with a wooden floor.

image::/img/da_shed/08_frame.jpg[Frame,width=640,height=480]

This frame did have a mistake: I didn't put anything in between the joists, in particular, where the seams of the plywood floor is hung.
As a result, I eventually added a second layer of 3/4" plywood flooring.
(With the 1 1/2" of total depth, the floor is crazy stable, but probably doesn't need the extra depth.)

image::/img/da_shed/09_floor.jpg[Floor,width=640,height=480]

Next step, was to assemble the wall frames.

image::/img/da_shed/10_frames.jpg[Frames,width=640,height=480]

Then, I went back to Home Depot, and rented a framing nailer.
This was when things got fun.
I actually was able to fasten everything just by myself.

image::/img/da_shed/11_framed.jpg[Frames Up,width=640,height=480]

From here, the next step was to cut and hang the rafters.
This is where things slowed down a bit, just because I had to cut everything using just a circular saw and a speed square.

image::/img/da_shed/12_rafters.jpg[Rafters,width=640,height=480]

The next stage was the roof sheathing, a.k.a. death wish #1.
It honestly wasn't too bad, but doing this solo probably isn't recommended for safety.

image::/img/da_shed/13_roof-sheathing.jpg[Roof Sheathing,width=640,height=480]

After this, it was time to cut and hang some panels.
The door was also constructed out of another panel at this point.
I ended up renting a finishing nailer for this, which I strongly recommend.

At this point, I also started with the roof underlayment.
That stuff is so heavy it just needed some staples but was pretty easy to do.

image::/img/da_shed/14_panels_and_roof_base.jpg[Side Panels and Roof Underlayment,width=640,height=480]

This was another fun Home Depot screw up: they ordered a ridge vent for me, and then told me they didn't have it.
So I went into the store, and lo and behold, there was the ridge vent I ordered... _WITH MY NAME ON IT_.

So this day, I was able to hang the roof, attach the ridge, and finish framing up the sides.

image::/img/da_shed/15_finished_roof.jpg[Roof Complete,width=640,height=480]

At this stage, I insulated the walls.
Now that it's cold, I really, really recommend doing this.

image::/img/da_shed/16_insulation.jpg[Insulation,width=640,height=480]

Finally got around to hanging the door and cutting windows.
(There's three windows, actually, just one in the front.)

image::/img/da_shed/17_door_and_window.jpg[Door and Window,width=640,height=480]

At this point, I needed to prepare for running electrical wiring.
Step 1 was to rent a jack hammer and take out some concrete slabs.

image::/img/da_shed/18_concrete.jpg[Concrete Demo,width=480,height=640]

And then step 2 was to dig a 20" trench from my house to the shed.
(This sucked.)

The wiring went easily enough though, the electricians took about a half day to get a new panel in there.

image::/img/da_shed/19_wiring.jpg[Wiring,width=480,height=640]

The final product?
A jam packed tiny little woodshop.

Here's some shots of the stuff.
Like a bandsaw, bench, dust collector, Shopsmith, planer.

And all kinds of jigs in progress.

image::/img/da_shed/20_bandsaw.jpg[Bandsaw,width=640,height=480]
image::/img/da_shed/21_dust_collector.jpg[Dust Collector,width=640,height=480]
image::/img/da_shed/22_planer.jpg[Shopsmith and Planer,width=640,height=480]

Overall, the lighting is perfect, and it heats up fast so I can work during the winter.
(For the summer, I have a little AC unit I need to connect... I have time to get to that yet.)

Sure, I could use about twice as much room (or more), but I'd like to have twice as much yard too, so there's that.







= What Bothers Me About The Cloud
Tristan Juricek <mr.tristan@gmail.com>
2019-02-23
:tags: technology
:keywords: Tristan Juricek,technology,software
:description: I use the cloud, but have concerns.

There seems to be two common opinions of _The Cloud_ these days:

1. https://en.wikipedia.org/wiki/Technopoly[Technopoly], as in, "Why would you use anything else?"
2. Denial, such as, https://blog.codinghorror.com/the-cloud-is-just-someone-elses-computer/["The Cloud Is Just Someone Else's Computer."]

I don't really agree with either viewpoint.

newthought:denialists[The denialists who just see the cloud as a fancy VM host] seem to be missing out on the really reliable managed services becoming available.
While yes, at first, cloud providers were a way to launch servers and easily configure your network, there's now a very compelling suite of tech being built on top of one another.
S3 would be my first example.

S3 is designed to be https://aws.amazon.com/s3/faqs/#Durability_.26_Data_Protection[99.999999999% durable].
What this means, is that if you had 10,000 files, you would expect to lose one those files once in _ten million years_.
By default, everything backs up in multiple physical locations, requiring some kind of massive, catastrophic failure, such as the government collapsing, to really put your data at risk.

Thus, if you build systems on top of s3, you're gaining an unbelievable amount of reliability.
And Amazon is doing this.

There is no doubt in my mind that their managed service products such as Aurora, Kinesis, etc, often start by bringing in other systems on top of this insanely reliable data storage system.
Their expertise results in fast turnarounds, and seems to be accelerating the breadth of their portfolio of managed services.
While many of these services start off lackluster, they often gain experience and are able to become compelling solutions.

Just think, at what point does it make sense to maintain a MySQL cluster these days vs Aurora?
My rough guess is that 9 out of 10 small to midsize companies would just see benefit of Aurora.
And that's what scares me.

newthought:technopolists[This is where the Technopolists] get their justification.
AWS can usually provide a lower TCO by basically hiring experts on their managed services.
They are the 10,000 lb gorilla in a wide array of technologies.
Ergo, if you're a business owner, do you want to rent from affordable experts with a proven track record, or try to hire a great team to manage your own systems?
For most, AWS or GCP or Azure ends up being just easier to get things done... and herein lies the rub.

Many of these managed services, such as Aurora, start their lives as open source products.
Most of these open source products use a support-style business approach.
You can get the tool for free, but if you hit any problems, you hire the main company for support.

The best part, though, is that you can hire _others_ for support.
And this occurs for long-lived technologies.
Throw a rock, or, a simple web search, and you can find tons of MySQL consultants to help you with your DB problems.
Thus, the open source ecosystem seems to create more, smaller businesses.

The Cloud, however, doesn't seem to have anyone but massive, huge players.
Amazon, Google, Microsoft, they're all so good at deploying things on their infrastructure, it changes the numbers for open source projects significantly.
https://www.techrepublic.com/article/why-redis-labs-made-a-huge-mistake-when-it-changed-its-open-source-licensing-strategy/[Some, like Redis], are trying to find ways to extract some money from the cloud players who sell their products for free.

The more I learn about the business side, the more the cloud seems like a way for huge companies to use open source as a loss-leader without the loss.
These big players don't seem to contribute to development or early promotion of a tool or technology.
They just sit back and wait for the next big thing to drop, and when it's popular, they can invest money, probably even able to predict a profit margin.
We all seem to be looking at the short term benefit, without the long term view.
Most of our tech will just end up getting owned by one of the major cloud vendors.

newthought:nextsteps[Maybe I'll end up being wrong,] and, open source technologies will flourish in the cloud world.
It just doesn't look like that right now.

I fear we're at a peak of open source business growth potential, with the coming age of the cloud ready to consolidate everything into the revenue streams of the big three players.
Though maybe, just maybe, the sheer size of the big cloud vendors will get in their own way.
But most seem to have figured out how to scale.
Thus, they operate like portfolio of many smaller businesses, and the size and deep pockets give them almost infinite power to hire the best and brightest.

It frightens me a little, to be honest.
Because we're really talking about big companies getting bigger.
But that kind of thing breeds monoculture.
You are either in The Show, or you are paying rent to The Show.
There may be no third option. At least nothing a rational person would choose.

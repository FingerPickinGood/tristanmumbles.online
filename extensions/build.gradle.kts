plugins {
    kotlin("jvm") version "1.3.72"
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    api(kotlin("stdlib-jdk8"))
    compileOnly("org.asciidoctor:asciidoctorj:2.0.0")
}

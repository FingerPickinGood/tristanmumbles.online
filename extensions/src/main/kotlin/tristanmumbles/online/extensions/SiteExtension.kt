package tristanmumbles.online.extensions

import org.asciidoctor.Asciidoctor
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry

class SiteExtension : ExtensionRegistry {

    override fun register(asciidoctor: Asciidoctor?) {
        val registry = asciidoctor?.javaExtensionRegistry() ?: error("unable to load javaExtensionRegistry")
        registry.inlineMacro("newthought", NewthoughtProcessor::class.java)
    }
}
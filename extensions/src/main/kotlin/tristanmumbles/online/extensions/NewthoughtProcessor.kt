package tristanmumbles.online.extensions

import org.asciidoctor.ast.ContentNode
import org.asciidoctor.extension.InlineMacroProcessor

class NewthoughtProcessor(configName: String, options: MutableMap<String, Any>) : InlineMacroProcessor(configName, options) {

    constructor(configName: String) : this(configName, mutableMapOf())

    override fun process(parent: ContentNode?, target: String?, attributes: MutableMap<String, Any>?): Any {
        val text = attributes!!["1"] as String

        // TODO: investigate how this can be properly generated using the
        // asciidoctor Ruby extension syntax
        return "<span class='newthought'>$text</span>"
    }
}
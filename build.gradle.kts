plugins {
    id("org.asciidoctor.jvm.convert") version "3.1.0"
}

repositories {
    mavenCentral()
    jcenter()
}

asciidoctorj {
    docExtensions(project(":extensions"))
}

tasks {
    "asciidoctor"(org.asciidoctor.gradle.jvm.AsciidoctorTask::class) {
        resources {
            from("src/docs/resources")
            into(".")
        }
        options(mapOf(
                "template_dirs" to listOf(file("src/docs/asciidoc-backend").absolutePath)
        ))
    }
}
